<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return 'Hello! This is tracking adwords request system';
});
$app->group(['prefix' => 'service'], function($router) {
    $router->post('/tracking-request', ['uses' => 'IndexController@trackingRequest']);
    $router->get('/remove-black-list', ['uses' => 'IndexController@removeBlackList']);
    $router->get('/get-black-list', ['uses' => 'IndexController@getBlackList']);
});
