<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 6/5/2019
 * Time: 09:58
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{
    public $timestamps = true;
    protected $table = 'black_list';
    protected $fillable = ['ip', 'site', 'campaign_id', 'is_deleted'];
}