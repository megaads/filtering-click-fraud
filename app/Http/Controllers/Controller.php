<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function getIP()
    {
        $retVal = 'UNKNOWN';
        if (key_exists("HTTP_CLIENT_IP", $_SERVER))
            $retVal = $_SERVER['HTTP_CLIENT_IP'];
        else if (key_exists("HTTP_X_FORWARDED_FOR", $_SERVER))
            $retVal = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (key_exists("HTTP_X_FORWARDED", $_SERVER))
            $retVal = $_SERVER['HTTP_X_FORWARDED'];
        else if (key_exists("HTTP_FORWARDED_FOR", $_SERVER))
            $retVal = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (key_exists("HTTP_FORWARDED", $_SERVER))
            $retVal = $_SERVER['HTTP_FORWARDED'];
        else if (key_exists("REMOTE_ADDR", $_SERVER))
            $retVal = $_SERVER['REMOTE_ADDR'];
        return $retVal;
    }

    protected function curlRequest($url, $data = [], $method = "GET", $isAsync = false)
    {
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($channel, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($channel, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($channel, CURLOPT_MAXREDIRS, 3);
        curl_setopt($channel, CURLOPT_POSTREDIR, 1);
        curl_setopt($channel, CURLOPT_TIMEOUT, 10);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 10);
        if ($isAsync) {
            curl_setopt($channel, CURLOPT_NOSIGNAL, 1);
            curl_setopt($channel, CURLOPT_TIMEOUT_MS, 400);
        }
        $response = curl_exec($channel);
        return $response;
    }
}
