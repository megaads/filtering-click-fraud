<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 6/5/2019
 * Time: 09:54
 */

namespace App\Http\Controllers;


use App\BlackList;
use App\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function trackingRequest (Request $request) {
        header('Access-Control-Allow-Origin: *');
        $response = [
            'status' => 'fail',
            'message' => 'Invalid params!'
        ];
        if ($request->input('url')) {
            if (!$this->checkIgnoreUserAgent()) {
                $url = base64_decode($request->input('url'));
                $data = [
                    'url' => $url,
                    'ip' => $this->getIP(),
                    'user_agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
                    'site' => $this->getSiteFromUrl($url),
                    'campaign_id' => $this->getCampaignIdFromUrl($url),
                    'refer_url' => $request->input('refer'),
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime(),
                ];
                \App\Request::insert($data);
                $isIpBlack = $this->checkIsIpBlack($data);
                $this->handleBlackList($data, $isIpBlack);
            }
            $response = [
                'status' => 'successful',
            ];
        }
        return response()->json($response);
    }

    private function getSiteFromUrl ($url) {
        $parse = parse_url($url);
        $retVal = isset($parse['host']) ? $parse['host'] : null;
        return $retVal;
    }

    private function getCampaignIdFromUrl ($url) {
        $retVal = null;
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
            if (isset($query['campaignid'])) {
                $retVal = intval($query['campaignid']);
            }
        }
        return $retVal;
    }

    private function checkIsIpBlack ($data) {
//        $ip = $data['ip'];
//        $db = new \IP2Proxy\Database();
//        $db->open(storage_path('/proxy/IP2PROXY-IP-PROXYTYPE-COUNTRY.BIN'), \IP2Proxy\Database::MEMORY_CACHE);
//        $ipInfo = $db->getAll($ip);
//        if (isset($ipInfo['isProxy']) && $ipInfo['isProxy'] == 2) {
//            return true;
//        }
//        $countRequest = \App\Request::where('site', '=', $data['site'])
//            ->where('campaign_id', '=', $data['campaign_id'])
//            ->where('ip', '=', $data['ip'])
//            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-10 seconds')))
//            ->count();
//        if ($countRequest > 5) {
//            return true;
//        }

        $sameRequests = \App\Request::where('site', '=', $data['site'])
            ->where('campaign_id', '=', $data['campaign_id'])
            ->where('user_agent', '=', $data['user_agent'])
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-3 hours')))
            ->get();
        if (count($sameRequests) > 0) {
            $listIp = [];
            $listIpB = [];
            foreach ($sameRequests as $request) {
                $ip = $request->ip;
                if (!in_array($ip, $listIp)) {
                    $listIp[] = $ip;
                }
                $arrIp = explode('.', $ip);
                if (!in_array($arrIp[0] . '.' . $arrIp[1], $listIpB)) {
                    $listIpB[] = $arrIp[0] . '.' . $arrIp[1];
                }
            }
            if (count($sameRequests) > 8 && count($listIp) / count($sameRequests) > 0.95 && count($listIpB) / count($listIp) < 0.5) {
//                $this->requestHandleIpBlock($data['site'], implode(',', $listIp), $data['campaign_id'], 'add');
                return true;
            }
        }


        return false;
    }

    private function requestHandleIpBlock ($site, $ips, $campaignId, $type) {
        $config = config('blacklist');
        if (isset($config[$site]['url_' . $type . '_ip_block'])) {
            $url = $config[$site]['url_' . $type . '_ip_block'];
            if (strpos($url, '?') !== false) {
                $url .= '&campaignId=' . $campaignId . '&ips=' . $ips;
            } else {
                $url .= '?campaignId=' . $campaignId . '&ips=' . $ips;
            }
            $this->curlRequest($url, [], 'GET', true);
        }
    }

    private function checkIsBlacklistInConfig ($data) {
        $retVal = false;
        $config = config('conditionBlacklist');
        if (isset($config[$data['site']])) {
            $conditions = $config[$data['site']];
            foreach ($conditions as $condition) {

            }
        }
    }

    private function handleBlackList ($data, $isBlackIp) {
        $item = BlackList::where('ip', '=', $data['ip'])
            ->where('site', '=', $data['site'])
            ->where('campaign_id', '=', $data['campaign_id'])
            ->where('is_deleted', '=', 0)
            ->first();
        $arrIp = explode('.', $data['ip']);
        $ip = $arrIp[0] . '.' . $arrIp[1] . '.0.0/16';
        if ($isBlackIp && !isset($item->id) && $data['campaign_id'] != 2074912008) {
            BlackList::insert([
                'ip' => $data['ip'],
                'site' => $data['site'],
                'campaign_id' => $data['campaign_id'],
                'is_deleted' => 0,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);
            $this->requestHandleIpBlock($data['site'], $ip, $data['campaign_id'], 'add');
        }
        if (!$isBlackIp && isset($item->id)) {
            $item->is_deleted = 1;
            $item->save();
            $this->requestHandleIpBlock($data['site'], $ip, $data['campaign_id'], 'remove');
        }
    }

    public function removeBlackList (Request $request) {
        $blackLists = BlackList::where('is_deleted', '=', 0)->get();
        $count = 0;
        foreach ($blackLists as $item) {
            $checkExistsRequest = \App\Request::where('site', '=', $item->site)
                ->where('campaign_id', '=', $item->campaign_id)
                ->where('ip', '=', $item->ip)
                ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-3 hours')))
                ->exists();
            if (!$checkExistsRequest) {
                $item->is_deleted = 1;
                $item->save();
                $count++;
                $arrIp = explode('.', $item->ip);
                $ip = $arrIp[0] . '.' . $arrIp[1] . '.0.0/16';
                $this->requestHandleIpBlock($item->site, $ip, $item->campaign_id, 'remove');
            }
        }

        return response()->json([
            'status' => 'successful',
            'message' => 'Deleted ' . $count . ' rows'
        ]);
    }

    public function getBlackList (Request $request) {
        $query = BlackList::query();
        if ($request->input('campaign_id')) {
            $query->where('campaign_id', '=', $request->input('campaign_id'));
        }
        if ($request->input('site')) {
            $query->where('site', '=', $request->input('site'));
        }
        $query->groupBy('ip');
        $query->orderBy('id', 'desc');
        $ips = $query->pluck('ip')->toArray();
        return response()->json([
            'status' => 'successful',
            'data' => $ips
        ]);
    }

    private function checkIgnoreUserAgent () {
        $retVal = false;
        $listPartUserAgent = ['AdsBot-Google', 'Google-Read-Aloud'];
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
        if ($userAgent) {
            foreach ($listPartUserAgent as $part) {
                if (strpos($userAgent, $part) !== false) {
                    $retVal = true;
                    break;
                }
            }
        }
        return $retVal;
    }
}