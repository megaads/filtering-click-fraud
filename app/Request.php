<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 6/5/2019
 * Time: 09:58
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $table = 'request';

    public $timestamps = true;

    protected $fillable = ['url', 'ip', 'user_agent', 'site', 'campaign_id', 'refer_url'];
}